Utility AI & GOAP Submission


Intro: Since I was under the delusion that I would have two otherwise final-less days to grind on this project, I drastically over-scoped (I even made and implemented animations for the character... *sigh*). Nevertheless, my idea for the Utility AI + GOAP combination is pretty well defined by the pseudocode in the primary AI script, MoonManController.


Project Description: This AI simulation follows a handful of astronauts shipwrecked on a distant moon as they deal with the physical and mental stress of repairing their ship. The "Moon Men" are motivated by physical health (sickness, tiredness, hunger, thirst), mental health (social, stress), and their job (plus its attending specific utilities). 


Detailed Description of What You're Looking At:


As I mentioned above, I drew and implemented some animations for the eponymous silhouetted moon men, and I implemented an A* package with the animations, so you're able to set their agent destination in script. 

As for my AI--the Utility AI & GOAP--that's all in the MoonManController attached to the Agent Game Object. 

My ambition was to stitch together Utility AI and GOAP in a way that plays to the strengths of both: the multidimensional maximization of UAI and the multi-stage action planning of GOAP. To that end, I devised a system that would allow UAI to decide which general activity to perform and GOAP to decide what specific actions should be taken to accomplish that goal. I also garnished it with the ability to suspend actions in order to undertake other actions, though there's obviously a comically wide gulf between jotting that down in pseudocode and actually making it work convincingly. 

The general process is as follows:

(1) Sense: The unit senses its surroundings and updates its utility metrics. If there has been a significant change in either its surroundings or its utility metrics, it will immediately move to step (2), Decide. If not, the unit checks to see if it is in the middle of any actions. If it is, it moves straight to step (3), Act. If it's not currently acting but has actions that it previously suspended, it will make those its current action and also jump to Act.

(2) Decide: This is the most complicated aspect of the algorithm. 

	(2.1) First, it checks to see if the unit is currently performing an activity. If it is, it will still perform the rest of the steps to determine the new best course of action. Then, it will compare those to the activity currently being performed, and if they're *significantly* better, they'll shove the current activity off to the backlog and switch to the new one. 

	(2.2) Next, it performs the main UAI step. For each activity (for instance, "relax", "socialize", etc.), it analyzes the aggregate utility that would be gained from performing that activity using a simplified metric. For instance, "relax" might be estimated to improve your mental health and physical health, while work might reduce your mental health but increase your job productivity. It picks the activity that provides the most utility given the current metrics and passes it off to step 2.3.

	(2.3) Next, it performs the GOAP step. Given the winning activity, it determines a finite goal such as "find the nearest ally and strike up a conversation" (necessary for the iterative GOAP process). Then, GOAP will go through 3 rounds of analyzing every action that it is able to perform until it hits the goal node. It will add every path that reaches a goal node to a dictionary of possible paths, and it will also estimate the utility of following that path.

		Example: the Utility AI determined that "chop down a tree" was the best course of action. GOAP will analyze all actions and pick any that are possible/have their conditions satisfied (You can't start with "chop down a tree" because you don't have an axe. You can get an axe. You can also take a nap.) For each action that is possible, it will analyze the *next* possible actions, and do this 3 times. This could leave you with multiple correct paths: for instance, "get axe">"chop down tree" is a valid path. So is "take nap">"get axe">"chop down tree", and so is "get axe">"take nap">"chop down tree". It will then roughly analyze the utility of every path to the goal. "Get axe">"chop down tree" is definitely the most efficient in terms of job productivity, but "get axe">"take nap">"chop down tree" gives you more mental and physical health.)

	(2.4) Next--and this is the key part--it passes all of those possible paths (and their estimated utilities) *back* to UAI. UAI analyzes each of them and selects the one that most maximizes the utility space as the *actual* series of actions to be undertaken. It sends them to (3), Act.

		Example: Taking the "chop down a tree" example, we can imagine that the UAI receives two possible courses of action: "get axe">"chop down tree" and "get axe">"take nap">"chop down tree". It would compare the benefits of each to the current state of the metrics; if job performance is very low, then maybe the former option is better, but if the unit is extremely tired and stressed, maybe taking a nap in the middle is the actual optimal course of action. 

(3) Act: Sort of self-explanatory in the pseudocode--just do the next action in the action list, then invoke (1) Sense. 

