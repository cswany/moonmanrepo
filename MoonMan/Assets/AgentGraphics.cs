﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class AgentGraphics : MonoBehaviour
{
    public AIPath aiPath;

    public Animator animator;

    void FixedUpdate()
    {
        if(aiPath.desiredVelocity.x < 0f)
        {
            transform.localScale = new Vector3(-1f * Mathf.Abs(transform.localScale.x), transform.localScale.y, 1f);
        }
        else if(aiPath.desiredVelocity.x > 0f)
        {
            transform.localScale = new Vector3(1f * Mathf.Abs(transform.localScale.x), transform.localScale.y, 1f);
        }

        if(Mathf.Abs(aiPath.desiredVelocity.x) + Mathf.Abs(aiPath.desiredVelocity.y) > 0)
        {
            animator.SetBool("moving", true);
        }
        else
        {
            animator.SetBool("moving", false);
        }
    }
}
