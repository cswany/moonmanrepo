﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class MoonManController : MonoBehaviour
{
    public AIDestinationSetter destinationSetter;

    public Transform walkTarget;
    // Start is called before the first frame update
    void Start()
    {
        destinationSetter.target = walkTarget;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Pseudocode:

    //****

    //Sense:

        //StartSense()
            
            //Get all AI objects (units, destinations) within a certain radius
            
            //Add those units to the appropriate sense lists

            //Calculate all utility metrics (physical health, mental health, job)

            //if (my sense metrics have departed significantly from what they were last time I acted || one of my utility metrics has changed drastically)
                //decide

            //else if (I am doing an action)
                //Act();
            
            //else
                //if (backupActivity and backupAction is empty)
                    //Invoke("StartSense", 1f)

                //else
                    //make those activities and actions the current activities and actions
                    //Act();
    
    //****

    //Decide:

        //AISense()

            //if (I am not currently performing an action)

                //STEP 1: Initial Utility AI

                    //foreach (Activity activity in activityList)

                        //Calculate the *aggregate* utility that this activity would have across all utility metrics
                        
                        //Add that score to utilityScoreByActivityIndex

                    //Pick the winningActivity based on its total utilityScoreByActivityIndex

                    //Pass winning activity on to GOAP

                //STEP 2: GOAP: 

                    //STEP 2.0: using winningActivity, determine an appropriate goal

                        //if (winningActivity == do your job)

                            //then the goapGoal is the next job that needs to be done in your profession

                        //if (winningActivity == socialize)

                            //then goapGoal is to find a crew member and socialize

                        //if (winningActivity == relax)

                            //then goapGoal is to find the nearest rest spot (personal tent or mess hall) and stay there for a while

                        //if (winningActivity == heal)

                            //then goapGoal is to go to the medical tent

                    //STEP 2.5: use GOAP to find all paths to the solution within 3 action steps
                    
                        //clear the GOAP planner (Dictionary<List<Action>, idealUtilityScores[int physical, int mental, int job]>)

                        //foreach (Action action in actionList)

                            //if (I can perform this action (Do I meet all of the requirements? e.g. for mining rocks, "Do I have a pickaxe?"))

                                //if (goal has been reached)
                                    //add it to the GOAP planner
                                    //calculate its idealUtilityScores
                                    //break;

                                //else
                                
                                    //foreach (Action action in actionList)

                                        //if (I can perform this action)
                                            
                                            //if (goal has been reached)
                                                //add it to the GOAP planner
                                                //calculate its idealUtilityScores
                                                //break;

                                            //else
                                            
                                                //foreach (Action action in actionList)

                                                    //if (I can perform this action)

                                                        //if (goal has been reached)
                                                        //add it to the GOAP planner
                                                        //calculate its idealUtilityScores
                                                        //break;
                                                    
                                                        //else
                                                        
                                                            //break;

                                                    //else

                                                        //break;
                                        
                                        //else

                                            //break;


                            //else
                                
                                //break;


                //STEP 3: Back to Utility AI

                    //Give Utility AI the GOAP Planner dictionary

                        //foreach KeyValuePair in the dictionary

                            //keep track of how much its idealUtilityScores would change the current utility space

                    //Pick the list of actions that most optimizes the utility space. 

                    //Send that list to Act()
            
            
            //else

                //foreach (Activity activity in activityList)

                    //Calculate the *aggregate* utility that this activity would have across all utility metrics
                    
                    //Add that score to utilityScoreByActivityIndex
                
                //if (the winning score is *significantly* higher than the utility score for the current action)

                    //Add the current actionList to backupActionList

                    //Add the current activity to backupActivity

                    //remove currentAction and currentActivity

                    //AISense();

                //else

                    //continue doing that action
                    //Invoke("StartSense", 1f)


    //****

    //Act:

        //Act() //receives a list from Step3 of Decide, or already has a list

            //add the dictionary's list to currentActionList

            //if (an action is in progress)

                //call relevant functions to perform those actions (for instance, the DestinationSetter attached to this gameObject will let you move it)

            //if (an action on the currentActionList is complete)

                //pop it off the list and move onto the next action

            //Invoke("StartSense", 1f);

    //****

    //Additional methods:

    //****


}